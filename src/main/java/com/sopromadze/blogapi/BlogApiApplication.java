package com.sopromadze.blogapi;

import com.sopromadze.blogapi.security.JwtAuthenticationFilter;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.data.convert.Jsr310Converters;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication
@EntityScan(basePackageClasses = { BlogApiApplication.class, Jsr310Converters.class })

public class BlogApiApplication {
	private static final Logger logger = LoggerFactory.getLogger(BlogApiApplication.class);
	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(BlogApiApplication.class);
//		SpringApplication.run(BlogApiApplication.class, args);
		Environment env = app.run(args).getEnvironment();
		logger.info("==============================================================");
		logger.info("Application :  {} "," APP-SERVICE");
		logger.info("Enviroment port:  {} ",env.getProperty("server.port"));
		logger.info("==============================================================");
	}

	@PostConstruct
	void init() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

	@Bean
	public JwtAuthenticationFilter jwtAuthenticationFilter() {
		return new JwtAuthenticationFilter();
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

}
